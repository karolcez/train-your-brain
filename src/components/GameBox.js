import React from 'react';

const GameBox = ({ boxSet, boxClickHandler, boxIndex }) => {

    const mathExpression = boxSet.mathExpressionsList[boxIndex];

    return (
        <div className="gameBox" onClick={() => boxClickHandler(boxSet.boxSetId)}>
            {mathExpression}
            {boxSet.mathExpressionsList.length > (boxIndex + 1)
                && <GameBox
                    boxSet={boxSet}
                    boxIndex={boxIndex + 1}
                    boxClickHandler={() => {}}
                />}
        </div>
    );
}

export default GameBox;
