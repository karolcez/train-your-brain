import React from 'react';

const Modal = ( {exitModal, children} ) => {

    return (
        <div className="modal">
            <div className="modalContent">
                <button onClick={exitModal} className="closeButton">
                    <span>&times;</span></button>
                {children}
            </div>
        </div>
    )

}

export default Modal;

