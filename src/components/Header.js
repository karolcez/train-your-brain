import React from 'react';

const Header = ({ newGameHandler, showHelpHandler }) => {

    return (
        <header>
            <div>
                <h1>TrainYourBrain</h1>
                <div className="emptyFlexboxItem"></div>
                <button onClick={newGameHandler}>New Game</button>
                <button onClick={showHelpHandler}>Help</button>
            </div>

        </header>
    );
}

export default Header;
