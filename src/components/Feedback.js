import React from 'react';
import * as gameStateTypes from "../gameLogic/gameStates";

const Feedback = ({ playerScore, timeLeft, gameState, expressionResult, bestScore }) => {
    return (
        <div id="gameFeedback">
            {(gameState == gameStateTypes.ROUND) &&
                [<div id="playerScore">
                    <p>{`Your score: ${playerScore}`}</p>
                </div>,
                <div id="timeCounter">
                    <p>{`Time left: ${timeLeft}s`}</p>
                </div>]
            }
            {(gameState == gameStateTypes.NEW_GAME) &&
                <div id="startGameFeedback">
                    <p>Choose expression with the highest result.</p>
                </div>
            }
            {(gameState == gameStateTypes.SHOW_SCORE) &&
                <div id="choosenExpressionResultFeedback">
                    <p>{`Chosen expression result: ${expressionResult}`} </p>
                </div>
            }
            {(gameState == gameStateTypes.GAME_OVER) &&
                <div id="gameOverFeedback">
                    <p>{`Game over. Your score: ${playerScore}. Optimal score: ${bestScore}`} </p>
                </div>
            }
        </div>
    );
}

export default Feedback;
