import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainActions from '../actions/actions';
import Feedback from "./Feedback";
import GameBox from "./GameBox";
import {
    checkGameOverConitions,
    countMathExpressions,
    nextStepAfterClick
} from "../gameLogic/gameLogic";
import * as gameState from "../gameLogic/gameStates";
import PropTypes from 'prop-types';

export class Main extends Component {
    constructor() {
        super();
        this.state = {
            expressionResult: 0
        }
        this.boxClickHandler = this.boxClickHandler.bind(this);
    }

    boxClickHandler(chosenBoxSetId) {
        const expressionsResults = countMathExpressions(this.props.actions, this.props.boxSets, chosenBoxSetId);
        this.setState({
            expressionResult: expressionsResults[chosenBoxSetId]
        })
        nextStepAfterClick(this.props.actions, this.props.intervalId, this.props.roundNumber);
    }

    componentWillUpdate() {
        checkGameOverConitions(this.props);
    }

    render() {
        const gameBoxSetClass = !(this.props.gameState == gameState.ROUND) ? "contentDisabled" : "";

        return (
            <main>
                <Feedback
                    playerScore={this.props.score}
                    bestScore={this.props.bestScore}
                    timeLeft={this.props.timeLeft}
                    gameState={this.props.gameState}
                    expressionResult={this.state.expressionResult}
                />

                <div className="mainGameContent">
                    <div className="flexboxContainer">
                        <div id="gameBoxSet1" className={gameBoxSetClass}>
                            <GameBox
                                boxClickHandler={this.boxClickHandler}
                                boxSet={this.props.boxSets[0]}
                                boxIndex={0} />
                        </div>

                        <div id="gameBoxSet2" className={gameBoxSetClass}>
                            <GameBox
                                boxClickHandler={this.boxClickHandler}
                                boxSet={this.props.boxSets[1]}
                                boxIndex={0} />
                        </div>
                    </div>

                    <div className="flexboxContainer">
                        <div id="gameBoxSet3" className={gameBoxSetClass}>
                            <GameBox
                                boxClickHandler={this.boxClickHandler}
                                boxSet={this.props.boxSets[2]}
                                boxIndex={0} />
                        </div>

                        <div id="gameBoxSet4" className={gameBoxSetClass}>
                            <GameBox
                                boxClickHandler={this.boxClickHandler}
                                boxSet={this.props.boxSets[3]}
                                boxIndex={0} />
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}

Main.propTypes = {
    boxSets: PropTypes.array.isRequired,
    gameState: PropTypes.string.isRequired,
    timeLeft: PropTypes.number.isRequired,
    intervalId: PropTypes.number.isRequired,
    score: PropTypes.number.isRequired,
    bestScore: PropTypes.number.isRequired,
    roundNumber: PropTypes.number.isRequired,
    boxSets: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
  };

function mapStateToProps(state) {

    return {
        ...state
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(mainActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Main);
