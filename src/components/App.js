import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from "../styles/style.css";
import Header from "./Header";
import Footer from "./Footer";
import Main from "./Main";
import Modal from "./Modal";
import * as mainActions from '../actions/actions';
import { startNewGame } from "../gameLogic/gameLogic";
import * as gameStateTypes from "../gameLogic/gameStates";
import PropTypes from 'prop-types';

class App extends Component {
  constructor() {
    super();
    this.newGameHandler = this.newGameHandler.bind(this);
    this.showHelpHandler = this.showHelpHandler.bind(this);
    this.exitHelpHandler = this.exitHelpHandler.bind(this);
    this.state = {
      showHelpModal: false
    }
  }

  newGameHandler() {
    startNewGame(
      this.props.actions,
      this.props.intervalId,
      this.props.gameState
    )
  }

  showHelpHandler() {
    this.setState({
      showHelpModal: true
    });
  }

  exitHelpHandler() {
    this.setState({
      showHelpModal: false
    });
  }

  componentWillMount() {
    startNewGame(
      this.props.actions,
      this.props.intervalId,
      gameStateTypes.START_GAME
    )
  }

  render() {
    const contentSetClass = (this.state.showHelpModal) ? "contentDisabled" : "";

    return (
      <div>
        <div className={contentSetClass}>
          <Header
            newGameHandler={this.newGameHandler}
            showHelpHandler={this.showHelpHandler}
          />
          <Main />
          <Footer />
        </div>
        {
          this.state.showHelpModal &&
          <Modal exitModal={this.exitHelpHandler}>
            <p>
              Calculate in mind math expressions from 4 box sets.
              Click this with the highest result.
              You have only 5s to make decision.
              Chosen math result will be added to your score at top.
              Try to achieve the highest score.
              When the game ends compare your score with the optimal score.
            </p>
            <img src={require("../img/helpExample.png")} alt="Math expression example."/>
            <p>
              This box set is equivalent to math expression: 10 + 2 - 3 + 13 = 22.
            </p>
          </Modal>
        }
      </div>

    );
  }
}

App.propTypes = {
  boxSets: PropTypes.array.isRequired,
  gameState: PropTypes.string.isRequired,
  timeLeft: PropTypes.number.isRequired,
  intervalId: PropTypes.number.isRequired,
  score: PropTypes.number.isRequired,
  bestScore: PropTypes.number.isRequired,
  roundNumber: PropTypes.number.isRequired,
  boxSets: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {

  return {
    ...state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(mainActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
