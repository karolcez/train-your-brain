import * as types from '../actions/actionTypes';

const initialState = {
    gameState: "",
    timeLeft: 0,
    intervalId: 0,
    score: 0,
    bestScore: 0,
    roundNumber: 0,
    boxSets: [
        { boxSetId: 0, mathExpressionsList: [] },
        { boxSetId: 1, mathExpressionsList: [] },
        { boxSetId: 2, mathExpressionsList: [] },
        { boxSetId: 3, mathExpressionsList: [] }
    ]
};


export default function (state = initialState, action) {
    switch (action.type) {
        case types.ADD_SCORE:
            return {
                ...state,
                score: state.score + action.payload
            };

        case types.ADD_BEST_SCORE:
            return {
                ...state,
                bestScore: state.bestScore + action.payload
            };

        case types.SUBSTRACT_TIME_LEFT:
            return {
                ...state,
                timeLeft: Math.round((state.timeLeft - 0.1) * 10) / 10
            };

        case types.SET_INTERVAL_ID:
            return {
                ...state,
                intervalId: action.payload
            };

        case types.SET_GAME_STATE:
            return {
                ...state,
                gameState: action.payload
            };

        case types.NEW_GAME:
            state = {
                ...state,
                timeLeft: 5.0,
                score: 0,
                bestScore: 0,
                roundNumber: 0,
                boxSets: [
                    { boxSetId: 0, mathExpressionsList: [] },
                    { boxSetId: 1, mathExpressionsList: [] },
                    { boxSetId: 2, mathExpressionsList: [] },
                    { boxSetId: 3, mathExpressionsList: [] }
                ]

            }
            return JSON.parse(JSON.stringify(state));

        case types.NEXT_ROUND:
            state.timeLeft = 5.0;
            state.roundNumber++;

            for (let index = 0; index < action.payload.length; index++) {
                state.boxSets[index].mathExpressionsList.push(action.payload[index]);

            }
            return JSON.parse(JSON.stringify(state));


        default:
            return state;

    }
}