export const START_GAME = "START_GAME";
export const NEW_GAME = "NEW_GAME";
export const ROUND = "ROUND";
export const SHOW_SCORE = "SHOW_SCORE";
export const GAME_OVER = "GAME_OVER";