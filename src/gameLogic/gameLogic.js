import * as gameStateTypes from "./gameStates";
import { FIRST_ROUND_START_TIME, NEXT_ROUND_START_TIME } from "./globals";


function randomMathExpression(isFirstRound) {
    const mathSighn = !isFirstRound && (Math.random() > 0.5 ? "+" : "-");
    return mathSighn + Math.round(Math.random() * 20);
}

export function createNextRound(isFirstRound) {
    const newMathExpressions = [];
    for (let index = 0; index < 4; index++) {
        newMathExpressions.push(randomMathExpression(isFirstRound));

    }
    return newMathExpressions;
}

export function startNewGame(actions, intervalId, gameState) {
    if (gameStateTypes.NEW_GAME != gameState &&
        gameStateTypes.SHOW_SCORE != gameState
    ) {
        actions.newGame();
        actions.setGameState(gameStateTypes.NEW_GAME);
        actions.nextRound(createNextRound(true));
        clearInterval(intervalId);
        setTimeout(() => {
            actions.setGameState(gameStateTypes.ROUND);
            const newIntervalId = setInterval(() => actions.substractTimeLeft(), 100);
            actions.setIntervalId(newIntervalId);
        }, FIRST_ROUND_START_TIME);
    }
}

export function checkGameOverConitions({ timeLeft, gameState, intervalId, actions }) {
    if ((timeLeft <= 0) && (gameState == gameStateTypes.ROUND)) {
        clearInterval(intervalId);
        actions.setGameState(gameStateTypes.GAME_OVER);
    }
}
export function countMathExpressions(actions, boxSets, chosenBoxSetId) {

    actions.setGameState(gameStateTypes.SHOW_SCORE);

    const expressionsResults = boxSets.map(e => {
        return e.mathExpressionsList
            .reduce((sum, elem) => { return eval(sum + elem) }, 0);
    })

    actions.addScore(expressionsResults[chosenBoxSetId]);
    actions.addBestScore(Math.max(...expressionsResults));

    return expressionsResults;
}

export function nextStepAfterClick(actions, intervalId, roundNumber) {
    setTimeout(() => {
        clearInterval(intervalId);

        if (roundNumber < 4) {
            actions.nextRound(createNextRound(false));

            const newIntervalId = setInterval(() => actions.substractTimeLeft(), 100);
            actions.setIntervalId(newIntervalId);
            actions.setGameState(gameStateTypes.ROUND);

        }
        else {

            actions.setGameState(gameStateTypes.GAME_OVER);
        }
    }, NEXT_ROUND_START_TIME);
}