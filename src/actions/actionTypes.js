export const ADD_SCORE = 'ADD_SCORE';
export const ADD_BEST_SCORE = 'ADD_BEST_SCORE';
export const NEW_GAME = 'NEW_GAME';
export const NEXT_ROUND = 'NEXT_ROUND';
export const SUBSTRACT_TIME_LEFT = 'SUBSTRACT_TIME_LEFT';
export const SET_INTERVAL_ID = 'SET_INTERVAL_ID';
export const SET_GAME_STATE = 'SET_GAME_STATE';
