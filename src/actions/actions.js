import * as types from './actionTypes';


export function addScore(payload) {
   return function(dispatch) {       
           return dispatch({ type: types.ADD_SCORE, payload });
   }      
}

export function addBestScore(payload) {
   return function(dispatch) {       
           return dispatch({ type: types.ADD_BEST_SCORE, payload });
   }      
}

export function substractTimeLeft() {
   return function(dispatch) {       
           return dispatch({ type: types.SUBSTRACT_TIME_LEFT });
   }      
}

export function setIntervalId(payload) {
   return function(dispatch) {       
           return dispatch({ type: types.SET_INTERVAL_ID, payload });
   }      
}

export function setGameState(payload) {
   return function(dispatch) {       
           return dispatch({ type: types.SET_GAME_STATE, payload });
   }      
}

export function newGame() {
   return function(dispatch) {       
           return dispatch({ type: types.NEW_GAME});
   }      
}

export function nextRound(payload) {
   return function(dispatch) {       
           return dispatch({ type: types.NEXT_ROUND, payload });
   }      
}
